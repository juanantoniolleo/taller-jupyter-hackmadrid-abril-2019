# Taller Jupyter HackMadrid Abril 2019

### INSTRUCCIONES:

#### Para seguir el taller es conveniente tener instalada la suite ANACONDA:

https://www.anaconda.com/distribution/

* ATENCION: OCUPA MUCHO ESPACIO Y TARDA EN DESCARGAR E INSTALAR
     
* DISPONIBLE PARA PYTHON 2.7 Y 3.7, DESCARGAR DESDE AQUÍ LA ULTIMA VERSIÓN
     
* IMPORTANTE: SELECCIONAR EL SISTEMA OPERATIVO CORRESPONDIENTE:
     
* OPCIONES: LINUX, MACOS O WINDOWS:

    
https://www.anaconda.com/distribution/

#### SI TIENES POCO ESPACIO O LO VAS A INSTALAR EN EL TALLER, ESTA ES LA OPCION:

#### Python 2 ó 3:

DESCARGAR DESDE LA ULTIMA VERSIÓN PARA EL SISTEMA OPERATIVO CORRESPONDIENTE:
    
https://www.python.org/downloads/

Python 3:
    
https://www.python.org/downloads/release/python-373/

Python 2:
    
https://www.python.org/downloads/release/python-2716/

#### Jupyter Notebook:
DESCARGAR DESDE:
    
https://jupyter.org/

* PARA INSTALARLO, LO PRIMERO INSTALAR PYTHON
    
* Y DESPUES, SEGUIR LAS INSTRUCCIONES:

    
https://jupyter.org/install.html


#### OTRA OPCION:

Jupyter Notebook también puede usarse desde la página web siguiente:

https://jupyter.org/try

* Pero tiene algunos inconvenientes, puede no funcionar por sobrecarga y no permitir hacer cosas que sí que se pueden hacer en local.
