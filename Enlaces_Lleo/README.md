## Enlaces_Lleo

* COLECCION DE ENLACES, POR REPASAR.
* ALGUNOS YA ESTAN PASADOS A OTRAS CARPETAS

Taller_Jupyter-Abril2019-Enlaces_Lleo01.txt

* GITLAB: Taller Jupyter HackMadrid
https://gitlab.com/juanantoniolleo/taller-jupyter-hackmadrid-abril-2019


ANUNCIO: 
¡ATENCION! Los que queráis seguir el taller de Jupyter Notebook con el portátil, por favor, repasad las instrucciones y si queréis algunos materiales desde esta dirección:
Taller Jupyter HackMadrid
https://gitlab.com/juanantoniolleo/taller-jupyter-hackmadrid-abril-2019

* SERVIDOR DE PRUEBAS:

http://lleo.alastria.eu/

* MI ACCESO DIRECTO:

http://10.147.17.214/

http://10.147.17.214/nextcloud/index.php/login

ARTICULOS:
* PYTHON TOOLS - HACKERS ONLINE CLUB:

https://hackersonlineclub.com/python-tools/
	* 

LIBRERIAS DE CIBERSEGURIDAD:
### SCAPY:
https://scapy.net

###  pcap: In the field of computer network administration, pcap is an application programming interface (API) for capturing network traffic. While the name is an abbreviation of a technical term of art (jargon) packet capture, that is not the API's proper name. 
Unix-like systems implement pcap in the libpcap library; for Windows, there is a port of libpcap named WinPcap that is no longer supported or developed, and a port named Npcap for Windows 7 and later that is still supported.

	https://en.wikipedia.org/wiki/Pcap
	
### python-libpcap:
https://sourceforge.net/projects/pylibpcap/

On Ubuntu/Debian systems you can install the libpcap development package in order to get the headers:

apt-get install libpcap-dev

* EJEMPLOS: EN

pylibpcap\pylibpcap-0.6.4\examples

findalldevs.py:
	
	
### sniff.py:
FUNCIONA COMO COMANDO, LANZANDOLO COMO SUDO:
Example to sniff all HTTP traffic on eth0 interface:
sudo ./sniff.py eth0 "port 80"

###  WINDOWS
https://github.com/orweis/winpcapy


### PYTHON CYBERSEC:
“Up to date” Python Guides for Pentesting/CyberSec

https://www.reddit.com/r/HowToHack/comments/acobqn/up_to_date_python_guides_for_pentestingcybersec/
Not a book but zaid's udemy course shows some basics in python3. You can find some of the scripts updated to Python 3 for Violent Python on Github, if that helps. Maybe these?
	
### LIBROS (AMAZON):

#### Python Penetration Testing Cookbook: Practical recipes on implementing information gathering, network security, intrusion detection, and post-exploitation

https://www.amazon.co.uk/dp/1784399779/ref=cm_sw_r_cp_api_i_4QjmCbBQ0EA5M

#### Python for Offensive PenTest:A practical guide to ethical hacking and penetration testing using Python

https://www.amazon.co.uk/dp/1788838971/ref=cm_sw_r_cp_api_i_oSjmCb249MPN7

- I recall seeing these in the previous Humble Bundle Books Bundle. Many people dislike Packt because there is no QA/quality check. Is this true for these books (assuming you used them)

#### Programming languages for cybersec (esp prototyping):
 
https://www.reddit.com/r/cybersecurity/comments/avcl9r/programming_languages_for_cybersec_esp_prototyping/

* If I had to learn one language for infosec it would be python. It can run on almost any OS, and you can do just about anything you want with it. It isn't always the proper solution, but you can make network analysis tools, fuzzers, poc apps, log filters, etc.
* Overall, I'd say Python. As a small amount of evidence, search "security programming" on Amazon, perhaps the first 50 results. Of the titles that mention a programming language: one C#, one PowerShell (also mentioned with Kali Linux and Windows), and a dozen titles with Python in the title.
* Bash Scripting, Python, Perl, Javascript, SQL and probably C/C++. I also can't imagine doing real hacking without knowing at least some Assembly language.
* Python and SQLas the main ones. .Net, Javascript, and PHP would also be useful.
* Research says that essential languages are C/C++ for low-level applications and Python/Java for high-level. Also web tech such as JS, PHP and SQL for DBs. But I'd like to know the consensus of this community + examples of stuff developed with these languages.

#### How is Python used in cyber security?
https://www.quora.com/How-is-Python-used-in-cyber-security


#### Python programming for digital forensics and security analysis: http://opensourceforu.com/2016/11/python-programming-digital-forensics-security-analysis/

SELECCION:
* NETWORK PORT SCANNING:
Generally, the nmap tool is used for the implementation of network port scanning, but using Python socket programming, it can be implemented without any third party tool. In Kali Linux, there are many tools available for digital forensics related to networks, but many of these implementations can be done using Python programming with just a few lines of instruction. The code for port scanning of any IP address can be downloaded from:

http://opensourceforu.com/article_source_code/sept16/digital_forensic.zip

The code checks which particular ports are open from the PortList [20, 22, 23, 80, 135, 445, 912]. Each value in the PortList specifies a particular service associated with the network.

-------------------------------------------------------
#### Python IS best programming langauge for hacking and security.

IF YOU WANT MORE INFORMATION SUBSCRIBE US ON YOUTUBE:
Technical Ustaad

FOLLOW US ON GOOGLE+:

Technical Ustaad - Google+

VIST US ON BLOGGER:

TechUst
-------------------------------------------------------
#### Python can be really useful for Tooling, Automation, and Shell Scripting — covering almost major python programming usage for cyber security. Python is also popular for Data Analysis (e.g. analysing web server logs), or Big Data. Here’s a fun game you can play on Big Data:
	
https://pixelastic.github.io/pokemonorbigdata/

### MLIRAS: VARIOS EJEMPLOS DE CIBERSEGURIDAD CON PYTHON:

https://github.com/mliras

	* 2 PROGRAMAS: SSID_listener.py Y spider.py

	* NECESITA SCAPY

	https://github.com/mliras/Cybersec

### JSERRATS - CYBERSEC: 6 PROGRAMAS EN PYTHON (Y EN SHELL)

https://github.com/jserrats

https://github.com/jserrats/cybersec

#### REPASAR SOBRE TODO ESTE:

	https://github.com/jserrats/cybersec/blob/master/source/mitm.py

jserrats_cybersec

#### BREAKING INTO CYBER SECURITY: PROGRAMMING LANGUAGES - Joakim Kakaei

http://acornaspirations.com/breaking-into-cyber-security/

PYTHON
God, where do I even start? I LOVE this language. If you are new to programming and want to pick it up as a skill, then I’d suggest starting with this language. Not only is the syntax very easy to understand (when you get past the friggin’ indentations), but it also has a seamless amount of libraries to do basically anything you would want to do.
You can do anything from multi-threaded programming to use its libraries to send TCP-packets to machines. The potential of the language is further increased by how easy it is to interface C-code into it, which combines the development speed of Python with the efficiency of C.
Python is also widely used when performing crypto- and malware-analysis. So if you want to work with that, you should have a look at Python! (I know, it’s awesome!).
In the case of learning Python I’d suggest looking up this website and use it together with the python docs. When you have a solid foundation and written one or a few medium-large programs I’d suggest getting a copy of Violent Python as it has a lot to teach. It’s mainly geared to the offensive security side of things, but if you understand how the offensive side thinks, you can use that to build defensive scripts too!
Trust me, if you don’t know this language – you should!

* SERVIDORES Y RECURSOS PARA PRACTICAR:

https://www.amanhardikar.com/mindmaps/Practice.html

https://www.hackthissite.org/

https://github.com/portantier/habu



* HACKING TOOLS WITH PYTHON:

https://resources.infosecinstitute.com/writing-hacking-tools-with-python-part-1/

https://resources.infosecinstitute.com/hacking-tools-with-python-part-2/#article

### VIOLENT PYTHON:

#### violent-python-py3:

https://gitlab.com/Galerion/violent-python-py3

####  violent_python

https://gitlab.com/jedore/violent_python

####  NO HAY NADA:
https://gitlab.com/Jandrusk/violent-python
